#! /bin/bash
set -x

DATA_PATH=/data

# Once a week, clean the old backups.
# Keep only one per week.
day=$(date +"%u")
if [[ day -eq 1 ]]; then
    # Keep the newest file older than 7 days.
    FILE_TO_KEEP=$(find $DATA_PATH/*.dump -mtime +7 -type f -exec ls -1t "{}" + | head -n 1)
    gzip $FILE_TO_KEEP

    # Delete the others.
    find $DATA_PATH/*.dump -mtime +7 -type f -delete;
fi

# PGPASSWORD: https://www.postgresql.org/docs/9.0/libpq-envars.html
export PGPASSWORD="$PG_PASSWORD"

filename="db_$(date +'%Y-%m-%d').dump"
pg_dump --user ${PG_USERNAME} --host ${PG_HOST} ${PG_DB} > $DATA_PATH/$filename

ls -alh $DATA_PATH
